from django import forms
from django.forms import ModelForm
from .models import Status

class StatusForm(ModelForm):
	error_messages = {
		'required': 'Tolong isi input ini',
	}

	title_attrs = {
		'type': 'text',
		'class': 'statusForm-input',
		'placeholder': 'Insert title'
	}

	text_attrs = {
		'type': 'text',
		'cols': 50,
		'rows': 4,
		'class': 'statusForm-textarea',
		'placeholder': 'What\'s on your mind?'
	}

	title = forms.CharField(label = 'Judul', required = True, max_length = 30,
		widget=forms.TextInput(attrs = title_attrs))
	text = forms.CharField(label = 'Pesan', required = True,
		widget=forms.Textarea(attrs=text_attrs))

	class Meta:
		model = Status
		fields = ('title', 'text',)