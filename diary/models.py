from django.db import models

# Create your models here.
class Status(models.Model):
	title = models.CharField(max_length = 30)
	text = models.TextField()
	date_posted = models.DateTimeField(auto_now_add = True)
