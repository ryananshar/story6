from django.test import TestCase
from django.test import Client
from django.urls import resolve
from .views import index, add
from .models import Status
from .forms import StatusForm

from selenium import webdriver
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.chrome.options import Options
import time


# Create your tests here.

class SimpleUnitTest(TestCase):
	def test_existing_url(self):
		response = Client().get('/')
		self.assertEqual(response.status_code, 200)

	def test_template(self):
		response = Client().get('')
		self.assertTemplateUsed(response, 'base.html')

	def test_landing_page_func(self):
		found = resolve('/')
		self.assertEqual(found.func, index)

	def test_add_func(self):
		found = resolve('/add/')
		self.assertEqual(found.func, add)

	def test_model(self):
		diary = Status.objects.create(title = 'aku', text = 'gabut')
		count_all = Status.objects.all().count()
		self.assertEqual(count_all, 1)

	def test_form_validation(self):
		form = StatusForm(data = {'text': ''})
		self.assertFalse(form.is_valid())
		self.assertEqual(
			form.errors['text'],
			["This field is required."])

	def test_post_success_and_render_result(self):
		test = 'Dummy'
		textTest = 'run'
		response_post = Client().post('/add/', {'title':test, 'text': textTest})
		self.assertEqual(response_post.status_code, 302)

		response = Client().get('')
		html_response = response.content.decode('utf8')
		self.assertIn(test, html_response)

	def test_post_error_and_render_result(self):
		test = 'Dummy'
		textTest = 'run'
		response_post = Client().post('/add/', {'title':'','text': ''})
		self.assertEqual(response_post.status_code, 200)

		response = Client().get('')
		html_response = response.content.decode('utf8')
		self.assertNotIn(test, html_response)

class Story6FunctionalTest(TestCase):

	def setUp(self):
		chrome_options = Options()
		chrome_options.add_argument('--dns-prefetch-disable')
		chrome_options.add_argument('--no-sandbox')  
		chrome_options.add_argument('--headless')
		chrome_options.add_argument('disable-gpu')
		self.selenium = webdriver.Chrome('./chromedriver', chrome_options=chrome_options)

	def tearDown(self):
		self.selenium.quit()

	def test_input_add(self):
		selenium = self.selenium
		selenium.get('http://127.0.0.1:8000/add/')

		title = selenium.find_element_by_id("id_title")
		text = selenium.find_element_by_id('id_text')
		submit = selenium.find_element_by_id('submit')

		title.send_keys('Coba selenium')
		text.send_keys('Mencoba membuat functional test selenium dengan chrome driver')
		submit.send_keys(Keys.RETURN)

		time.sleep(4)
		self.assertIn("Coba selenium", self.selenium.page_source)
