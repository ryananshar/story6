from django.shortcuts import render, redirect
from .models import Status
from .forms import StatusForm
# Create your views here.
def index(request):
	if(request.method == 'POST'):
		form = StatusForm(request.POST)
		pk = request.POST['id']
		obj = Status.objects.get(pk=pk)
		obj.delete()

	status = Status.objects.order_by('-date_posted')
	context = {
		'status' : status
	}
	return render(request, 'base.html', context)

def add(request):
	form = StatusForm(request.POST or None)
	if(request.method == 'POST' and form.is_valid()):
		pesan = Status(
			title = form.cleaned_data['title'],
			text = form.cleaned_data['text']
			)
		pesan.save()
		return redirect('home')
	else:
		form = StatusForm()

	context = {
		'form' : form
	}
	return render(request, 'add.html', context)