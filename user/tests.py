from django.test import TestCase
from django.test import Client
from django.urls import resolve
from .views import register, login, logout
from .forms import UserRegisterForm

from selenium import webdriver
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.chrome.options import Options
import time
# Create your tests here.

class UnitTest(TestCase):

	def test_url_login_exists(self):
		response = Client().get('/login/')
		self.assertEqual(response.status_code, 200)
		
	def test_url_logout_exists(self):
		response = Client().get('/logout/')
		self.assertEqual(response.status_code, 200)

	def test_url_that_does_not_exist(self):
		response = Client().get('/nothing/')
		self.assertEqual(response.status_code, 404)

	def test_use_form_template(self):
		response = Client().get('/')
		self.assertTemplateUsed(response, 'base.html')

	def test_is_inside_login(self):
		response = Client().get('/login/')
		content = response.content.decode('utf8')
		self.assertIn('Log In', content)
		self.assertIn('Username', content)
		self.assertIn('Password', content)
		self.assertIn('Login', content)
		self.assertIn('<button', content)

	def test_form_is_inside_login(self):
		response = Client().get('/login/')
		content = response.content.decode('utf8')
		self.assertIn('<form', content)

	def test_page_uses_views_function(self):
		found = resolve('/login/')
		self.assertEqual(found.func, login) #Views's function

	def test_form_validation(self):
		form = UserRegisterForm(data = {'username': ''})
		self.assertFalse(form.is_valid())
		self.assertEqual(
			form.errors['username'],
			["This field is required."])
	# def test_post_success_and_render_result(self):
	# 	test = 'sagaz'
	# 	key = 'wew123wew'
	# 	response_post = Client().post('/login/', {'username':test, 'password': key})
	# 	self.assertEqual(response_post.status_code, 302)

	# 	response = Client().get('')
	# 	html_response = response.content.decode('utf8')
	# 	self.assertIn(test, html_response)

# class SeleniumFunctionalTest(TestCase):

# 	def setUp(self):
# 		options = Options()
# 		options.add_argument('--headless')
# 		self.selenium = webdriver.Chrome('./chromedriver', chrome_options=chrome_options)
# 		super(SeleniumFunctionalTest, self).setUp()

# 	def tearDown(self):
# 		self.selenium.quit()
# 		super(SeleniumFunctionalTest,self).tearDown()

# 	def test_inserting_data_to_form(self):
# 		#Inserting one data
# 		self.selenium.get('http://127.0.0.1:8000/')
# 		time.sleep(10)

# 		signin_name = self.selenium.find_element_by_id("id_signin_user")
# 		signin_name.send_keys("name")
# 		signin_pass = self.selenium.find_element_by_id("id_signin_pass")
# 		signin_pass.send_keys("pass")
# 		signin = self.selenium.find_element_by_id("button_signin")
# 		signin.click()
# 		time.sleep(10)
# 		self.assertIn("Welcome", self.selenium.page_source)

# 		login_name = self.selenium.find_element_by_id("id_login_user")
# 		login_name.send_keys("name")
# 		login_pass = self.selenium.find_element_by_id("id_login_pass")
# 		login_pass.send_keys("pass")
# 		login = self.selenium.find_element_by_id("button_login")
# 		login.click()
# 		time.sleep(10)

# 		self.assertIn("Halo, name", self.selenium.page_source)
# 		self.assertIn("Logout", self.selenium.page_source)
