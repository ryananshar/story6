# from django.urls import path
# from . import views

from django.conf.urls import include
from django.urls import *
from django.urls import path, include
from django.contrib import admin
from django.contrib.auth import views as auth_views
from user import views as user_views

urlpatterns = [
	path('register/', user_views.register, name='register'),
	path('login/', user_views.login, name='login'),
	path('logout/', user_views.logout, name='logout'),
    # path('login/', auth_views.LoginView.as_view(template_name='login.html'), name='login'),
    # path('logout/', auth_views.LogoutView.as_view(template_name='logout.html'), name='logout'),
]