from django.shortcuts import render, redirect
from django.contrib import messages
from django.contrib.auth.decorators import login_required
from django.contrib.auth.models import User, auth
from diary.models import Status
from .forms import UserRegisterForm


def register(request):
    if request.method == 'POST':
        form = UserRegisterForm(request.POST)
        if form.is_valid():
            form.save()
            username = form.cleaned_data.get('username')
            request.session['name'] = username
            messages.success(request, f'Your account has been created! You are now able to log in')
            return redirect('login')
    else:
        form = UserRegisterForm()
    return render(request, 'register.html', {'form': form})

def login(request):
    if request.method == 'POST':
        username = request.POST['username']
        password = request.POST['password']
        request.session['name'] = username
        user = auth.authenticate(username=username, password=password)
        if user is not None:
            auth.login(request, user)
            status = Status.objects.order_by('-date_posted')
            name = request.session.get('name')
            context = {
            'status' : status, 'name' : name
            }
            return render(request, 'base.html', context)
            # return redirect('home')
        else:
            messages.info(request, 'Invalid information')
            return redirect('login')

    else:
        return render(request, 'login.html')

def logout(request):
	name = request.session.get('name')
	del request.session['name']
	auth.logout(request)
	return render(request, 'logout.html', { 'name': name })